﻿using AForge.Fuzzy;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace FuzzyRobotWinForm
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private PictureBox pbTerrain;
        private Button btnStep;
        private Button btnRun;
        private TextBox txtInterval;
        private CheckBox cbLasers;
        private GroupBox groupBox1;
        private Label txtRight;
        private Label txtLeft;
        private Label txtFront;
        private Label lbl;
        private Label label2;
        private Label label1;
        private GroupBox groupBox2;
        private Label label3;
        private Label txtAngle;
        private Label label4;
        private PictureBox pbRobot;
        private Button btnReset;

        private GroupBox gbComandos;

        private GroupBox groupBox3;

        private Label label5;

        private CheckBox cbTrajeto;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(MainForm));
            this.pbTerrain = new PictureBox();
            this.btnStep = new Button();
            this.btnRun = new Button();
            this.txtInterval = new TextBox();
            this.cbLasers = new CheckBox();
            this.groupBox1 = new GroupBox();
            this.txtRight = new Label();
            this.txtLeft = new Label();
            this.txtFront = new Label();
            this.lbl = new Label();
            this.label2 = new Label();
            this.label1 = new Label();
            this.groupBox2 = new GroupBox();
            this.label3 = new Label();
            this.txtAngle = new Label();
            this.gbComandos = new GroupBox();
            this.cbTrajeto = new CheckBox();
            this.btnReset = new Button();
            this.label4 = new Label();
            this.pbRobot = new PictureBox();
            this.groupBox3 = new GroupBox();
            this.label5 = new Label();
            ((ISupportInitialize)this.pbTerrain).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gbComandos.SuspendLayout();
            ((ISupportInitialize)this.pbRobot).BeginInit();
            this.groupBox3.SuspendLayout();
            base.SuspendLayout();
            this.pbTerrain.BackColor = SystemColors.ControlText;
            this.pbTerrain.ErrorImage = null;
            this.pbTerrain.InitialImage = null;
            this.pbTerrain.Location = new Point(160, 8);
            this.pbTerrain.Name = "pbTerrain";
            this.pbTerrain.Size = new Size(500, 500);
            this.pbTerrain.SizeMode = PictureBoxSizeMode.AutoSize;
            this.pbTerrain.TabIndex = 10;
            this.pbTerrain.TabStop = false;
            this.pbTerrain.MouseMove += new MouseEventHandler(this.pbTerrain_MouseMove);
            this.pbTerrain.MouseDown += new MouseEventHandler(this.pbTerrain_MouseDown);
            this.btnStep.FlatStyle = FlatStyle.Flat;
            this.btnStep.Location = new Point(6, 109);
            this.btnStep.Name = "btnStep";
            this.btnStep.Size = new Size(75, 23);
            this.btnStep.TabIndex = 14;
            this.btnStep.Text = "&One Step";
            this.btnStep.Click += new EventHandler(this.button3_Click);
            this.btnRun.FlatStyle = FlatStyle.Flat;
            this.btnRun.Location = new Point(6, 138);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new Size(75, 23);
            this.btnRun.TabIndex = 15;
            this.btnRun.Text = "&Run";
            this.btnRun.Click += new EventHandler(this.btnRun_Click);
            this.txtInterval.Location = new Point(6, 83);
            this.txtInterval.Name = "txtInterval";
            this.txtInterval.Size = new Size(72, 20);
            this.txtInterval.TabIndex = 16;
            this.txtInterval.Text = "10";
            this.txtInterval.TextAlign = HorizontalAlignment.Right;
            this.cbLasers.Checked = true;
            this.cbLasers.CheckState = CheckState.Checked;
            this.cbLasers.Location = new Point(8, 40);
            this.cbLasers.Name = "cbLasers";
            this.cbLasers.Size = new Size(120, 24);
            this.cbLasers.TabIndex = 17;
            this.cbLasers.Text = "&Show Beams";
            this.groupBox1.Controls.Add(this.txtRight);
            this.groupBox1.Controls.Add(this.txtLeft);
            this.groupBox1.Controls.Add(this.txtFront);
            this.groupBox1.Controls.Add(this.lbl);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new Point(8, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new Size(144, 72);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sensor readings::";
            this.txtRight.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.txtRight.Location = new Point(104, 48);
            this.txtRight.Name = "txtRight";
            this.txtRight.RightToLeft = RightToLeft.Yes;
            this.txtRight.Size = new Size(32, 16);
            this.txtRight.TabIndex = 29;
            this.txtRight.Text = "0";
            this.txtRight.TextAlign = ContentAlignment.MiddleLeft;
            this.txtLeft.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.txtLeft.Location = new Point(104, 32);
            this.txtLeft.Name = "txtLeft";
            this.txtLeft.RightToLeft = RightToLeft.Yes;
            this.txtLeft.Size = new Size(32, 16);
            this.txtLeft.TabIndex = 28;
            this.txtLeft.Text = "0";
            this.txtLeft.TextAlign = ContentAlignment.MiddleLeft;
            this.txtFront.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.txtFront.Location = new Point(104, 16);
            this.txtFront.Name = "txtFront";
            this.txtFront.RightToLeft = RightToLeft.Yes;
            this.txtFront.Size = new Size(32, 16);
            this.txtFront.TabIndex = 27;
            this.txtFront.Text = "0";
            this.txtFront.TextAlign = ContentAlignment.MiddleLeft;
            this.lbl.Location = new Point(8, 48);
            this.lbl.Name = "lbl";
            this.lbl.Size = new Size(100, 16);
            this.lbl.TabIndex = 26;
            this.lbl.Text = "Right (pixels):";
            this.label2.Location = new Point(8, 32);
            this.label2.Name = "label2";
            this.label2.Size = new Size(100, 16);
            this.label2.TabIndex = 25;
            this.label2.Text = "Left (pixels):";
            this.label1.Location = new Point(8, 16);
            this.label1.Name = "label1";
            this.label1.Size = new Size(88, 16);
            this.label1.TabIndex = 24;
            this.label1.Text = "Frontal (pixels):";
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtAngle);
            this.groupBox2.Location = new Point(8, 88);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new Size(144, 40);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Output:";
            this.label3.Location = new Point(8, 16);
            this.label3.Name = "label3";
            this.label3.Size = new Size(88, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "Angle (degrees):";
            this.txtAngle.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.txtAngle.Location = new Point(96, 16);
            this.txtAngle.Name = "txtAngle";
            this.txtAngle.Size = new Size(40, 16);
            this.txtAngle.TabIndex = 29;
            this.txtAngle.Text = "0,00";
            this.txtAngle.TextAlign = ContentAlignment.MiddleLeft;
            this.gbComandos.Controls.Add(this.cbTrajeto);
            this.gbComandos.Controls.Add(this.btnReset);
            this.gbComandos.Controls.Add(this.label4);
            this.gbComandos.Controls.Add(this.btnStep);
            this.gbComandos.Controls.Add(this.cbLasers);
            this.gbComandos.Controls.Add(this.btnRun);
            this.gbComandos.Controls.Add(this.txtInterval);
            this.gbComandos.Location = new Point(8, 136);
            this.gbComandos.Name = "gbComandos";
            this.gbComandos.Size = new Size(144, 200);
            this.gbComandos.TabIndex = 26;
            this.gbComandos.TabStop = false;
            this.gbComandos.Text = "Tools:";
            this.cbTrajeto.Location = new Point(8, 16);
            this.cbTrajeto.Name = "cbTrajeto";
            this.cbTrajeto.Size = new Size(120, 24);
            this.cbTrajeto.TabIndex = 19;
            this.cbTrajeto.Text = "&Track Path";
            this.btnReset.FlatStyle = FlatStyle.Flat;
            this.btnReset.Location = new Point(6, 167);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new Size(75, 23);
            this.btnReset.TabIndex = 0;
            this.btnReset.Text = "Rest&art";
            this.btnReset.Click += new EventHandler(this.btnReset_Click);
            this.label4.Location = new Point(6, 67);
            this.label4.Name = "label4";
            this.label4.Size = new Size(125, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Move Interval (ms):";
            this.pbRobot.BackColor = Color.Transparent;
            this.pbRobot.Location = new Point(216, 472);
            this.pbRobot.Name = "pbRobot";
            this.pbRobot.Size = new Size(10, 10);
            this.pbRobot.TabIndex = 11;
            this.pbRobot.TabStop = false;
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new Point(8, 342);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new Size(144, 119);
            this.groupBox3.TabIndex = 27;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Hints:";
            this.label5.Location = new Point(8, 16);
            this.label5.Name = "label5";
            this.label5.Size = new Size(125, 88);
            this.label5.TabIndex = 10;
            this.label5.Text = "Left click the image to draw passages (white), right click the image to draw walls (black).";
            this.AutoScaleBaseSize = new Size(5, 13);
            base.ClientSize = new Size(664, 513);
            base.Controls.Add(this.groupBox3);
            base.Controls.Add(this.gbComandos);
            base.Controls.Add(this.groupBox2);
            base.Controls.Add(this.groupBox1);
            base.Controls.Add(this.pbRobot);
            base.Controls.Add(this.pbTerrain);
            base.FormBorderStyle = FormBorderStyle.Fixed3D;
            base.MaximizeBox = false;
            base.Name = "MainForm";
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Fuzzy Auto Guided Vehicle Sample";
            ((ISupportInitialize)this.pbTerrain).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.gbComandos.ResumeLayout(false);
            this.gbComandos.PerformLayout();
            ((ISupportInitialize)this.pbRobot).EndInit();
            this.groupBox3.ResumeLayout(false);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        #endregion
    }
}

