﻿using AForge.Fuzzy;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FuzzyRobotWinForm
{
    public partial class MainForm : Form
    {
        private bool FirstInference;
        private int LastX;
        private int LastY;
        private double Angle;
        private string RunLabel;
        private Point InitialPos;
        private Bitmap OriginalMap;
        private Bitmap InitialMap;
        private InferenceSystem IS;
        private Thread thMovement;


        public MainForm()
        {
            this.InitializeComponent();

            this.pbTerrain.Image = Image.FromFile("pbTerrain.png");
            this.pbRobot.Image = Image.FromFile("pbRobot.png");

            this.Angle = 0.0;
            this.OriginalMap = new Bitmap(this.pbTerrain.Image);
            this.InitialMap = new Bitmap(this.pbTerrain.Image);
            this.InitFuzzyEngine();
            this.FirstInference = true;
            this.pbRobot.Top = this.pbTerrain.Bottom - 50;
            this.pbRobot.Left = this.pbTerrain.Left + 60;
            this.InitialPos = this.pbRobot.Location;
            this.RunLabel = this.btnRun.Text;
        }

        private void InitFuzzyEngine()
        {
            FuzzySet label = new FuzzySet("Near", new TrapezoidalFunction(15.0f, 50.0f, TrapezoidalFunction.EdgeType.Right));
            FuzzySet label2 = new FuzzySet("Medium", new TrapezoidalFunction(15.0f, 50.0f, 60.0f, 100.0f));
            FuzzySet label3 = new FuzzySet("Far", new TrapezoidalFunction(60.0f, 100.0f, TrapezoidalFunction.EdgeType.Left));
            LinguisticVariable linguisticVariable = new LinguisticVariable("RightDistance", 0.0f, 120.0f);
            linguisticVariable.AddLabel(label);
            linguisticVariable.AddLabel(label2);
            linguisticVariable.AddLabel(label3);
            LinguisticVariable linguisticVariable2 = new LinguisticVariable("LeftDistance", 0.0f, 120.0f);
            linguisticVariable2.AddLabel(label);
            linguisticVariable2.AddLabel(label2);
            linguisticVariable2.AddLabel(label3);
            LinguisticVariable linguisticVariable3 = new LinguisticVariable("FrontalDistance", 0.0f, 120.0f);
            linguisticVariable3.AddLabel(label);
            linguisticVariable3.AddLabel(label2);
            linguisticVariable3.AddLabel(label3);
            FuzzySet label4 = new FuzzySet("VeryNegative", new TrapezoidalFunction(-40.0f, -35.0f, TrapezoidalFunction.EdgeType.Right));
            FuzzySet label5 = new FuzzySet("Negative", new TrapezoidalFunction(-40.0f, -35.0f, -25.0f, -20.0f));
            FuzzySet label6 = new FuzzySet("LittleNegative", new TrapezoidalFunction(-25.0f, -20.0f, -10.0f, -5.0f));
            FuzzySet label7 = new FuzzySet("Zero", new TrapezoidalFunction(-10.0f, 5.0f, 5.0f, 10.0f));
            FuzzySet label8 = new FuzzySet("LittlePositive", new TrapezoidalFunction(5.0f, 10.0f, 20.0f, 25.0f));
            FuzzySet label9 = new FuzzySet("Positive", new TrapezoidalFunction(20.0f, 25.0f, 35.0f, 40.0f));
            FuzzySet label10 = new FuzzySet("VeryPositive", new TrapezoidalFunction(35.0f, 40.0f, TrapezoidalFunction.EdgeType.Left));
            LinguisticVariable linguisticVariable4 = new LinguisticVariable("Angle", -50.0f, 50.0f);
            linguisticVariable4.AddLabel(label4);
            linguisticVariable4.AddLabel(label5);
            linguisticVariable4.AddLabel(label6);
            linguisticVariable4.AddLabel(label7);
            linguisticVariable4.AddLabel(label8);
            linguisticVariable4.AddLabel(label9);
            linguisticVariable4.AddLabel(label10);
            Database database = new Database();
            database.AddVariable(linguisticVariable3);
            database.AddVariable(linguisticVariable2);
            database.AddVariable(linguisticVariable);
            database.AddVariable(linguisticVariable4);
            this.IS = new InferenceSystem(database, new CentroidDefuzzifier(1000));
            this.IS.NewRule("Rule 1", "IF FrontalDistance IS Far THEN Angle IS Zero");
            this.IS.NewRule("Rule 2", "IF FrontalDistance IS Far AND RightDistance IS Far AND LeftDistance IS Far THEN Angle IS Zero");
            this.IS.NewRule("Rule 3", "IF RightDistance IS Near AND LeftDistance IS Medium THEN Angle IS LittleNegative");
            this.IS.NewRule("Rule 4", "IF RightDistance IS Medium AND LeftDistance IS Near THEN Angle IS LittlePositive");
            this.IS.NewRule("Rule 5", "IF RightDistance IS Far AND FrontalDistance IS Near THEN Angle IS Positive");
            this.IS.NewRule("Rule 6", "IF LeftDistance IS Far AND FrontalDistance IS Near THEN Angle IS Negative");
            this.IS.NewRule("Rule 7", "IF RightDistance IS Far AND LeftDistance IS Far AND FrontalDistance IS Near THEN Angle IS Positive");
        }

        private void DoInference()
        {
            this.IS.SetInput("RightDistance", Convert.ToSingle(this.txtRight.Text));
            this.IS.SetInput("LeftDistance", Convert.ToSingle(this.txtLeft.Text));
            this.IS.SetInput("FrontalDistance", Convert.ToSingle(this.txtFront.Text));
            try
            {
                double num = this.IS.Evaluate("Angle");
                this.txtAngle.Text = num.ToString("##0.#0");
                this.Angle += num;
            }
            catch (Exception)
            {
            }
        }

        private void pbTerrain_MouseDown(object sender, MouseEventArgs e)
        {
            this.pbTerrain.Image = this.CopyImage(this.OriginalMap);
            this.LastX = e.X;
            this.LastY = e.Y;
        }

        private void pbTerrain_MouseMove(object sender, MouseEventArgs e)
        {
            Graphics graphics = Graphics.FromImage(this.pbTerrain.Image);
            Color color = Color.Yellow;
            if (e.Button == MouseButtons.Left)
            {
                color = Color.White;
            }
            else if (e.Button == MouseButtons.Right)
            {
                color = Color.Black;
            }
            if (color != Color.Yellow)
            {
                graphics.FillRectangle(new SolidBrush(color), e.X - 40, e.Y - 40, 80, 80);
                this.LastX = e.X;
                this.LastY = e.Y;
                graphics.DrawImage(this.pbTerrain.Image, 0, 0);
                this.OriginalMap = this.CopyImage(this.pbTerrain.Image as Bitmap);
                this.pbTerrain.Refresh();
                graphics.Dispose();
            }
        }

        private void GetMeasures()
        {
            this.pbTerrain.Image = this.CopyImage(this.OriginalMap);
            Bitmap bitmap = this.pbTerrain.Image as Bitmap;
            Point point = new Point(this.pbRobot.Left - this.pbTerrain.Left + 5, this.pbRobot.Top - this.pbTerrain.Top + 5);
            if (bitmap.GetPixel(point.X, point.Y).R == 0 && bitmap.GetPixel(point.X, point.Y).G == 0 && bitmap.GetPixel(point.X, point.Y).B == 0)
            {
                if (this.btnRun.Text != this.RunLabel)
                {
                    this.btnRun_Click(this.btnRun, null);
                }
                string text = "The vehicle is on the solid area!";
                MessageBox.Show(text, "Error!");
                throw new Exception(text);
            }
            Point obstacle = this.GetObstacle(point, bitmap, -1, 0);
            Point obstacle2 = this.GetObstacle(point, bitmap, 1, 90);
            Point obstacle3 = this.GetObstacle(point, bitmap, 1, -90);
            Graphics graphics = Graphics.FromImage(bitmap);
            if (this.cbLasers.Checked)
            {
                graphics.DrawLine(new Pen(Color.Green, 1f), obstacle, point);
                graphics.DrawLine(new Pen(Color.Red, 1f), obstacle2, point);
                graphics.DrawLine(new Pen(Color.Red, 1f), obstacle3, point);
            }
            if (this.btnRun.Text != this.RunLabel)
            {
                graphics.FillEllipse(new SolidBrush(Color.Navy), point.X - 5, point.Y - 5, 10, 10);
            }
            graphics.DrawImage(bitmap, 0, 0);
            graphics.Dispose();
            this.pbTerrain.Refresh();
            this.txtFront.Text = this.GetDistance(point, obstacle).ToString();
            this.txtLeft.Text = this.GetDistance(point, obstacle2).ToString();
            this.txtRight.Text = this.GetDistance(point, obstacle3).ToString();
        }

        private int GetDistance(Point p1, Point p2)
        {
            return Convert.ToInt32(Math.Sqrt(Math.Pow((double)(p1.X - p2.X), 2.0) + Math.Pow((double)(p1.Y - p2.Y), 2.0)));
        }

        private Point GetObstacle(Point Start, Bitmap Map, int Inc, int AngleOffset)
        {
            Point result = new Point(Start.X, Start.Y);
            double num = (this.Angle + 90.0 + (double)AngleOffset) * 3.1415926535897931 / 180.0;
            int num2 = 0;
            int num3 = 0;
            int num4 = 0;
            while (result.X + num2 >= 0 && result.X + num2 < Map.Width && result.Y + num3 >= 0 && result.Y + num3 < Map.Height && (Map.GetPixel(result.X + num2, result.Y + num3).R != 0 || Map.GetPixel(result.X + num2, result.Y + num3).G != 0 || Map.GetPixel(result.X + num2, result.Y + num3).B != 0))
            {
                num4 += Inc;
                num2 = Convert.ToInt32((double)num4 * Math.Cos(num));
                num3 = Convert.ToInt32((double)num4 * Math.Sin(num));
            }
            result.X += num2;
            result.Y += num3;
            return result;
        }

        private Bitmap CopyImage(Bitmap Src)
        {
            return new Bitmap(Src);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            this.Angle = 0.0;
            this.pbTerrain.Image = new Bitmap(this.InitialMap);
            this.OriginalMap = new Bitmap(this.InitialMap);
            this.FirstInference = true;
            this.pbRobot.Location = this.InitialPos;
            this.txtFront.Text = "0";
            this.txtLeft.Text = "0";
            this.txtRight.Text = "0";
            this.txtAngle.Text = "0,00";
        }

        private void MoveAGV()
        {
            double num = (this.Angle + 90.0) * 3.1415926535897931 / 180.0;
            int num2 = 0;
            int num3 = -4;
            num2 += num3;
            int num4 = Convert.ToInt32((double)num2 * Math.Cos(num));
            int num5 = Convert.ToInt32((double)num2 * Math.Sin(num));
            if (this.cbTrajeto.Checked)
            {
                Graphics graphics = Graphics.FromImage(this.OriginalMap);
                Point pt = new Point(this.pbRobot.Left - this.pbTerrain.Left + this.pbRobot.Width / 2, this.pbRobot.Top - this.pbTerrain.Top + this.pbRobot.Height / 2);
                Point pt2 = new Point(pt.X + num4, pt.Y + num5);
                graphics.DrawLine(new Pen(new SolidBrush(Color.Blue)), pt, pt2);
                graphics.DrawImage(this.OriginalMap, 0, 0);
                graphics.Dispose();
            }
            this.pbRobot.Top = this.pbRobot.Top + num5;
            this.pbRobot.Left = this.pbRobot.Left + num4;
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (button.Text == this.RunLabel)
            {
                button.Text = "&Stop";
                this.btnStep.Enabled = false;
                this.btnReset.Enabled = false;
                this.txtInterval.Enabled = false;
                this.cbLasers.Enabled = false;
                this.cbTrajeto.Enabled = false;
                this.pbRobot.Hide();
                this.StartMovement();
                return;
            }
            this.StopMovement();
            button.Text = this.RunLabel;
            this.btnReset.Enabled = true;
            this.btnStep.Enabled = true;
            this.txtInterval.Enabled = true;
            this.cbLasers.Enabled = true;
            this.cbTrajeto.Enabled = true;
            this.pbRobot.Show();
            this.pbTerrain.Image = this.CopyImage(this.OriginalMap);
            this.pbTerrain.Refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.pbRobot.Hide();
            this.AGVStep();
            this.pbRobot.Show();
        }

        private void StartMovement()
        {
            this.thMovement = new Thread(new ThreadStart(this.MoveCycle));
            this.thMovement.IsBackground = true;
            this.thMovement.Priority = ThreadPriority.AboveNormal;
            this.thMovement.Start();
        }

        private void MoveCycle()
        {
            try
            {
                while (Thread.CurrentThread.IsAlive)
                {
                    MethodInvoker method = new MethodInvoker(this.AGVStep);
                    base.BeginInvoke(method);
                    Thread.Sleep(Convert.ToInt32(this.txtInterval.Text));
                }
            }
            catch (ThreadInterruptedException)
            {
            }
        }

        private void AGVStep()
        {
            if (this.FirstInference)
            {
                this.GetMeasures();
            }
            try
            {
                this.DoInference();
                this.MoveAGV();
                this.GetMeasures();
            }
            catch (Exception)
            {
            }
        }

        private void StopMovement()
        {
            if (this.thMovement != null)
            {
                this.thMovement.Interrupt();
                this.thMovement = null;
            }
        }
    }
}
