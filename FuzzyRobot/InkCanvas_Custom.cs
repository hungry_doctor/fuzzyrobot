﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace FuzzyRobot
{
    public class InkCanvas_Custom : InkCanvas
    {
        private Point _dpi;

        private Point _robotLocation;
        private double _size;
        private double _halfSize;
        private Brush _robotBorderBrush;
        private Pen _robotPen;
        private Pen _obsticlesPen;
        private Point _centerPoint;

        public Point DPI { get { return _dpi; } }
        public double RobotSize { get { return _size; } }
        public double RobotHalfSize { get { return _halfSize; } }

        public Point RobotLocation
        {
            get
            {
                return _robotLocation;
            }
            set
            {
                _robotLocation = value;
                _centerPoint = new Point(value.X + _halfSize, value.Y + _halfSize);
            }
        }

        public float Angle { get; set; }


        public InkCanvas_Custom()
        {
            _size = 18.0;
            _halfSize = _size / 2.0;

            _robotBorderBrush = new SolidColorBrush(Colors.Purple);
            _robotBorderBrush.Freeze();

            _robotPen = new Pen(_robotBorderBrush, 1);
            _robotPen.Freeze();

            _obsticlesPen = new Pen(Brushes.ForestGreen, 1);
            _obsticlesPen.Freeze();

            PresentationSource source = PresentationSource.FromVisual(this);
            if (source != null)
            {
                _dpi = new Point(96.0 * source.CompositionTarget.TransformToDevice.M11, 96.0 * source.CompositionTarget.TransformToDevice.M22);
            }
            else
            {
                _dpi = new Point(96d, 96d);
            }
        }



        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            drawingContext.PushTransform(new RotateTransform(Angle, _centerPoint.X, _centerPoint.Y));
            drawingContext.DrawRectangle(null, _robotPen, new Rect(_robotLocation.X, _robotLocation.Y, _size, _size));
            drawingContext.Pop();
        }

    }
}
