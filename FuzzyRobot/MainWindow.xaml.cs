﻿using AForge.Fuzzy;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Ink;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace FuzzyRobot
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private State _state;
        private InferenceSystem _is;
        private DispatcherTimer _timer;
        private ConcurrentSet<Point> _borderPoints;

        private double _leftDistance;
        private double _rightDistance;
        private double _frontDistance;


        public MainWindow()
        {
            InitializeComponent();
            ResetRobot();

            _borderPoints = new ConcurrentSet<Point>();
            _state = State.Stopped;
            _timer = new DispatcherTimer()
            {
                Interval = new TimeSpan(0, 0, 0, 0, 25)
            };
            _timer.Tick += _timer_Tick;

            InitFuzzyEngine();
        }


        private void InitFuzzyEngine()
        {
            FuzzySet fuzzyNear = new FuzzySet("Near", new TrapezoidalFunction(25.0f, 60.0f, TrapezoidalFunction.EdgeType.Right));
            FuzzySet fuzzyMedium = new FuzzySet("Medium", new TrapezoidalFunction(25.0f, 60.0f, 70.0f, 110.0f));
            FuzzySet fuzzyFar = new FuzzySet("Far", new TrapezoidalFunction(70.0f, 110.0f, TrapezoidalFunction.EdgeType.Left));

            LinguisticVariable linguisticRight = new LinguisticVariable("RightDistance", 0.0f, 145.0f);
            linguisticRight.AddLabel(fuzzyNear);
            linguisticRight.AddLabel(fuzzyMedium);
            linguisticRight.AddLabel(fuzzyFar);
            LinguisticVariable liguisticLeft = new LinguisticVariable("LeftDistance", 0.0f, 145.0f);
            liguisticLeft.AddLabel(fuzzyNear);
            liguisticLeft.AddLabel(fuzzyMedium);
            liguisticLeft.AddLabel(fuzzyFar);
            LinguisticVariable linguisticFront = new LinguisticVariable("FrontalDistance", 0.0f, 145.0f);
            linguisticFront.AddLabel(fuzzyNear);
            linguisticFront.AddLabel(fuzzyMedium);
            linguisticFront.AddLabel(fuzzyFar);

            //LinguisticVariable linguisticAngle = new LinguisticVariable("Angle", -50.0f, 50.0f);
            //linguisticAngle.AddLabel(new FuzzySet("VeryNegative", new TrapezoidalFunction(-40.0f, -35.0f, TrapezoidalFunction.EdgeType.Right)));
            //linguisticAngle.AddLabel(new FuzzySet("Negative", new TrapezoidalFunction(-40.0f, -35.0f, -25.0f, -20.0f)));
            //linguisticAngle.AddLabel(new FuzzySet("LittleNegative", new TrapezoidalFunction(-25.0f, -20.0f, -10.0f, -5.0f)));
            //linguisticAngle.AddLabel(new FuzzySet("Zero", new TrapezoidalFunction(-10.0f, 5.0f, 5.0f, 10.0f)));
            //linguisticAngle.AddLabel(new FuzzySet("LittlePositive", new TrapezoidalFunction(5.0f, 10.0f, 20.0f, 25.0f)));
            //linguisticAngle.AddLabel(new FuzzySet("Positive", new TrapezoidalFunction(20.0f, 25.0f, 35.0f, 40.0f)));
            //linguisticAngle.AddLabel(new FuzzySet("VeryPositive", new TrapezoidalFunction(35.0f, 40.0f, TrapezoidalFunction.EdgeType.Left)));

            LinguisticVariable linguisticAngle = new LinguisticVariable("Angle", -40.0f, 40.0f);
            linguisticAngle.AddLabel(new FuzzySet("VeryNegative", new TrapezoidalFunction(-30.0f, -25.0f, TrapezoidalFunction.EdgeType.Right)));
            linguisticAngle.AddLabel(new FuzzySet("Negative", new TrapezoidalFunction(-30.0f, -25.0f, -15.0f, -10.0f)));
            linguisticAngle.AddLabel(new FuzzySet("LittleNegative", new TrapezoidalFunction(-15.0f, -10.0f, -5.0f, 1f)));
            linguisticAngle.AddLabel(new FuzzySet("Zero", new TrapezoidalFunction(-5.0f, 1f, 1f, 5f)));
            linguisticAngle.AddLabel(new FuzzySet("LittlePositive", new TrapezoidalFunction(1f, 5.0f, 10.0f, 15.0f)));
            linguisticAngle.AddLabel(new FuzzySet("Positive", new TrapezoidalFunction(10.0f, 15.0f, 25.0f, 30.0f)));
            linguisticAngle.AddLabel(new FuzzySet("VeryPositive", new TrapezoidalFunction(25.0f, 30.0f, TrapezoidalFunction.EdgeType.Left)));

            Database database = new Database();
            database.AddVariable(linguisticFront);
            database.AddVariable(liguisticLeft);
            database.AddVariable(linguisticRight);
            database.AddVariable(linguisticAngle);

            this._is = new InferenceSystem(database, new CentroidDefuzzifier(750));

            this._is.NewRule("Rule 1", "IF FrontalDistance IS Far THEN Angle IS Zero");
            this._is.NewRule("Rule 2", "IF FrontalDistance IS Far AND RightDistance IS Far AND LeftDistance IS Far THEN Angle IS Zero");
            this._is.NewRule("Rule 3", "IF RightDistance IS Near AND LeftDistance IS Medium THEN Angle IS LittleNegative");
            this._is.NewRule("Rule 4", "IF RightDistance IS Medium AND LeftDistance IS Near THEN Angle IS LittlePositive");
            this._is.NewRule("Rule 5", "IF RightDistance IS Far AND FrontalDistance IS Near THEN Angle IS Positive");
            this._is.NewRule("Rule 6", "IF LeftDistance IS Far AND FrontalDistance IS Near THEN Angle IS Negative");
            this._is.NewRule("Rule 7", "IF RightDistance IS Far AND LeftDistance IS Far AND FrontalDistance IS Near THEN Angle IS Positive");
        }



        private void Button_StopStart_Click(object sender, RoutedEventArgs e)
        {
            ChangeState();
        }

        private void Button_Clear_Click(object sender, RoutedEventArgs e)
        {
            canvas.Strokes.Clear();
        }

        private void Canvas_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var point = e.GetPosition(canvas);
            canvas.RobotLocation = new Point(point.X - canvas.RobotHalfSize, point.Y - canvas.RobotHalfSize);
            canvas.Angle = 0;
            canvas.InvalidateVisual();

            canvas.Dispatcher.Invoke(() => { }, DispatcherPriority.Render);
        }

        private void _timer_Tick(object sender, System.EventArgs e)
        {
            AGVStep();
            canvas.InvalidateVisual();
        }


        private void ChangeState()
        {
            if (_state == State.Running)
            {
                _timer.IsEnabled = false;
                this.Dispatcher.Invoke(() =>
                {
                    canvas.IsHitTestVisible = true;
                    button_Clear.IsEnabled = true;
                    button_StopStart.Content = "Старт";
                });
                canvas.InvalidateVisual();

                _state = State.Stopped;
            }
            else
            {
                _borderPoints.Clear();
                foreach (var stroke in canvas.Strokes)
                {
                    Geometry sketchGeo = stroke.GetGeometry();
                    Rect strokeBounds = sketchGeo.Bounds;

                    Parallel.For((int)strokeBounds.TopLeft.X, (int)strokeBounds.TopRight.X + 1, (x) =>
                    {
                        for (int y = (int)strokeBounds.TopLeft.Y; y < (int)strokeBounds.BottomLeft.Y + 1; y++)
                        {
                            Point p = new Point(x, y);

                            if (sketchGeo.FillContains(p))
                            {
                                _borderPoints.Add(p);
                            }
                        }
                    });
                }

                GetMeasures();
                _timer.IsEnabled = true;

                this.Dispatcher.Invoke(() =>
                {
                    canvas.IsHitTestVisible = false;
                    button_Clear.IsEnabled = false;
                    button_StopStart.Content = "Стоп";
                });
                _state = State.Running;
            }
        }

        private void ResetRobot()
        {
            canvas.RobotLocation = new Point(25.0, 25.0);
            canvas.Angle = 0;
        }


        private void AGVStep()
        {
            DoInference();
            MoveAGV();
            GetMeasures();
        }

        private void DoInference()
        {
            this._is.SetInput("RightDistance", (float)_rightDistance);
            this._is.SetInput("LeftDistance", (float)_leftDistance);
            this._is.SetInput("FrontalDistance", (float)_frontDistance);

            try
            {
                canvas.Angle += this._is.Evaluate("Angle");
            }
            catch
            {
            }
        }

        private void MoveAGV()
        {
            double num = (canvas.Angle + 90.0f) * Math.PI / 180.0;
            int num2 = 0;
            int num3 = -4;
            num2 += num3;
            int num4 = (int)(num2 * Math.Cos(num));
            int num5 = (int)(num2 * Math.Sin(num));

            canvas.RobotLocation = new Point(canvas.RobotLocation.X + num4, canvas.RobotLocation.Y + num5);
        }

        private void GetMeasures()
        {
            var location = new Point(canvas.RobotLocation.X, canvas.RobotLocation.Y);

            _frontDistance = GetDistance(location, GetObstacle(-1, 0));
            _leftDistance = GetDistance(location, GetObstacle(1, 90));
            _rightDistance = GetDistance(location, GetObstacle(1, -90));
        }

        private Point GetObstacle(int inc, double angleOffset)
        {
            Point result = new Point(canvas.RobotLocation.X, canvas.RobotLocation.Y);

            double num = (canvas.Angle + 90.0 + angleOffset) * Math.PI / 180.0;
            int num2 = 0;
            int num3 = 0;
            int num4 = 0;

            while (result.X + num2 > 0 && result.X + num2 < canvas.ActualWidth && result.Y + num3 > 0 && result.Y + num3 < canvas.ActualHeight && !_borderPoints.Contains(new Point((int)(result.X + num2), (int)(result.Y + num3))))
            {
                num4 += inc;
                num2 = (int)(num4 * Math.Cos(num));
                num3 = (int)(num4 * Math.Sin(num));
            }

            result.X += num2;
            result.Y += num3;

            return result;
        }

        private double GetDistance(Point p1, Point p2)
        {
            return Math.Sqrt(Math.Pow((p1.X - p2.X), 2.0) + Math.Pow((p1.Y - p2.Y), 2.0));
        }

    }
}
